﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RiversideHealth.Data;
using RiversideHealth.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using RiversideHealth.Models.AccountViewModels;
using RiversideHealth.Services;
using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;

namespace RiversideHealth.Controllers
{
    [Authorize]
    [Route("[controller]/[action]")]
    public class SuperAdminController : Controller
    {
        private readonly RiversideHealthDbContext db;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly ILogger _logger;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public SuperAdminController(
           UserManager<ApplicationUser> userManager,
           SignInManager<ApplicationUser> signInManager,
           IEmailSender emailSender,
           ILogger<AccountController> logger,
           RiversideHealthDbContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _logger = logger;
            db = context;
        }

        [TempData]
        public string ErrorMessage { get; set; }


        string connString = Global.connString; // @"Data Source = (localdb)\mssqllocaldb; Initial Catalog = RiversideHealth; Integrated Security=True";

        //Super Admin list view
        [HttpGet]
        public async Task<ActionResult> SuperAdminIndex()

        {
            //get current user details
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                //DataTable dbblUser = new DataTable();
                //using (SqlConnection sqlcon = new SqlConnection(connString))
                //{
                //    //check if current user is in AspNetUserRoles table
                //    sqlcon.Open();
                //    var cmd = sqlcon.CreateCommand();
                //    cmd.CommandText = "SELECT * FROM dbo.AspNetUserRoles where UserId =@userid ";
                //    cmd.Parameters.Add("@userid",SqlDbType.NVarChar).Value= user.Id;
                //    using (var reader = cmd.ExecuteReader())
                //    {
                //        dbblUser.Load(reader);
                //    }
                //    sqlcon.Close();
                //}

                ////if yes
                //if (dbblUser.Rows.Count > 0)
                //{
                //    //check if role is 1 = Superadmin

                //    //if yes
                //    if (dbblUser.Rows[0][1].ToString().Equals("1"))
                //    {
                //        //Get user details and display all users to superadmin
                //        DataTable dbblUsers = new DataTable();
                //        using (SqlConnection sqlcon = new SqlConnection(connString))
                //        {
                //            sqlcon.Open();
                //            //SqlDataAdapter sqlda = new SqlDataAdapter("SELECT dbo.AspNetUsers.*,dbo.Admins.* FROM dbo.AspNetUsers FULL Join dbo.Admins ON dbo.AspNetUsers.Adminid = dbo.Admins.Adminid", sqlcon);
                //            SqlDataAdapter sqlda = new SqlDataAdapter("SELECT dbo.AspNetUsers.* from dbo.AspNetUsers where Id !=@currentuserid", sqlcon);
                //            sqlda.Parameters.Add("@userid", SqlDbType.NVarChar).Value = user.Id;
                //            sqlda.Fill(dbblUsers);
                //            sqlcon.Close();
                //        }
                //        ViewData["IsSuperAdmin"] = "true";
                //        return View(dbblUsers);
                //    }
                //    else
                //    {
                //        //if no
                //        ViewData["IsSuperAdmin"] = "false";
                //        return View();
                //    }
                //}
                //else
                //{
                //    //if no

                //    //display error

                //    ViewData["IsSuperAdmin"] = "false";
                //    return View();

                //}

                //get user role
                var roles = await _userManager.GetRolesAsync(user);
                //var usersOfRole = await _userManager.GetUsersInRoleAsync("Superadmin");
                if (roles.Count() > 0 && roles != null)
                {
                    if (roles[0].ToUpper() == "SUPERADMIN")
                    {
                        ViewData["UserIsSuperAdmin"] = "true";
                        DataTable tblUser = new DataTable();
                        using (SqlConnection sqlcon = new SqlConnection(connString))
                        {
                            sqlcon.Open();
                            var cmd = sqlcon.CreateCommand();
                            //cmd.CommandText = "SELECT dbo.AspNetUsers.*,dbo.Admins.* FROM dbo.AspNetUsers FULL Join dbo.Admins ON dbo.AspNetUsers.Adminid = dbo.Admins.Adminid where dbo.AspNetUsers.Id =@userid ";
                            cmd.CommandText = "SELECT dbo.AspNetUsers.* from dbo.AspNetUsers where dbo.AspNetUsers.Id != @userid ";
                            cmd.Parameters.Add("@userid", SqlDbType.NVarChar).Value = user.Id;
                            using (var reader = cmd.ExecuteReader())
                            {
                                tblUser.Load(reader);
                            }
                            sqlcon.Close();
                        }
                        ViewData["IsSuperAdmin"] = "true";
                        return View(tblUser);
                    }
                    else
                    {
                        ViewData["UserIsSuperAdmin"] = "false";
                        return View();
                    }
                }

            }
           
            ViewData["IsSuperAdmin"] = "false";
            return View();
            
        }


        //Super Admin edit view 
        public async Task<ActionResult> SuperAdminEdit(string id)
        {

            //get current user details
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                //DataTable dbblUser = new DataTable();
                //using (SqlConnection sqlcon = new SqlConnection(connString))
                //{
                //    //check if current user is in AspNetUserRoles table
                //    sqlcon.Open();
                //    var cmd = sqlcon.CreateCommand();
                //    cmd.CommandText = "SELECT * FROM dbo.AspNetUserRoles where UserId =@userid ";
                //    cmd.Parameters.Add("@userid", SqlDbType.NVarChar).Value = user.Id;
                //    using (var reader = cmd.ExecuteReader())
                //    {
                //        dbblUser.Load(reader);
                //    }
                //    sqlcon.Close();
                //}

                ////if yes
                //if (dbblUser.Rows.Count > 0)
                //{
                //    //check if role is 1 = Superadmin
                //    if (dbblUser.Rows[0][1].ToString().Equals("1"))
                //    {
                //        DataTable tblUser = new DataTable();

                //        using (SqlConnection sqlcon = new SqlConnection(connString))
                //        {
                //            sqlcon.Open();
                //            var cmd = sqlcon.CreateCommand();
                //            cmd.CommandText = "SELECT dbo.AspNetUsers.*,dbo.Admins.* FROM dbo.AspNetUsers FULL Join dbo.Admins ON dbo.AspNetUsers.Adminid = dbo.Admins.Adminid where dbo.AspNetUsers.Id =@userid ";
                //            cmd.Parameters.Add("@userid", SqlDbType.NVarChar).Value = id;
                //            using (var reader = cmd.ExecuteReader())
                //            {
                //                tblUser.Load(reader);
                //            }
                //            sqlcon.Close();
                //        }
                //        ViewData["IsSuperAdmin"] = "true";
                //        return View(tblUser);
                //    }
                //}


                //get user role
                var roles = await _userManager.GetRolesAsync(user);

                if (roles.Count() > 0 && roles != null)
                {
                    //check if role is 1 = Superadmin
                    if (roles[0].ToUpper() == "SUPERADMIN")
                    {
                        ViewData["UserIsSuperAdmin"] = "true";

                        DataTable tblUser = new DataTable();
                        using (SqlConnection sqlcon = new SqlConnection(connString))
                        {
                            sqlcon.Open();
                            var cmd = sqlcon.CreateCommand();
                            //cmd.CommandText = "SELECT dbo.AspNetUsers.*,dbo.Admins.* FROM dbo.AspNetUsers FULL Join dbo.Admins ON dbo.AspNetUsers.Adminid = dbo.Admins.Adminid where dbo.AspNetUsers.Id =@userid ";
                            cmd.CommandText = "SELECT dbo.AspNetUsers.*,dbo.Admins.* " +
                                "FROM dbo.AspNetUsers " +
                                "full Join dbo.Admins " +
                                "ON dbo.AspNetUsers.id = dbo.Admins.UserId " +
                                "where dbo.AspNetUsers.Id= @userid ";
                            cmd.Parameters.Add("@userid", SqlDbType.NVarChar).Value = id;
                            using (var reader = cmd.ExecuteReader())
                            {
                                tblUser.Load(reader);
                            }
                            sqlcon.Close();
                        }
                        ViewData["IsSuperAdmin"] = "true";
                        return View(tblUser);
                    }
                    else
                    {
                        ViewData["UserIsSuperAdmin"] = "false";
                    }
                }
            }

            ViewData["IsSuperAdmin"] = "false";
            return View();
        }

        // POST: SuperAdminEdit/id=1
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SuperAdminEdit(IFormCollection collection)
        {
            var user = await GetCurrentUserAsync();

            //Check if user already exist or not in the Admin table
            using (SqlConnection sqlcon = new SqlConnection(connString))
            {
                sqlcon.Open();
                var cmd = sqlcon.CreateCommand();
                cmd.CommandText = "SELECT * FROM dbo.Admins where dbo.Admins.UserId =@userid ";
                cmd.Parameters.Add("@userid", SqlDbType.NVarChar).Value = collection["userid"].ToString();
                var reader = cmd.ExecuteReader();


                //if no -> insert the userid in admin table and update aspnetusers table for this new admin id
                if (!reader.HasRows)
                {
                    var status = collection["status"];
                    if (status == "Approved")
                    {
                        using (SqlConnection conn = new SqlConnection(connString))
                        {
                            conn.Open();
                            string query1 = "Insert into dbo.Admins (dbo.Admins.Fname,dbo.Admins.Lname,dbo.Admins.UserId) Values (@fname,@lname,@userid)";
                            SqlCommand sqlcmd = new SqlCommand(query1, conn);
                            sqlcmd.Parameters.AddWithValue("@fname", collection["firstName"].ToString());
                            sqlcmd.Parameters.AddWithValue("@lname", collection["lastName"].ToString());
                            sqlcmd.Parameters.AddWithValue("@userid", collection["userid"].ToString());
                            sqlcmd.ExecuteNonQuery();
                            sqlcmd.CommandText = "Select @@Identity"; //get the id on new inserted row
                            int newadminid = Convert.ToInt32(sqlcmd.ExecuteScalar());


                            //update aspnetusers table also with new admin id
                            string query2 = "Update dbo.AspNetUsers SET dbo.AspNetUsers.AdminID = @adminid where dbo.AspNetUsers.id=@userid ";
                            SqlCommand sqlcmd2 = new SqlCommand(query2, conn);
                            sqlcmd2.Parameters.AddWithValue("@adminid", newadminid);
                            sqlcmd2.Parameters.AddWithValue("@userid", collection["userid"].ToString());
                            sqlcmd2.ExecuteNonQuery();
                            conn.Close();
                        }
                    }
                    else
                    {
                        //insert the user fname & lname but don't update the admin id in the aspnetusers
                        using (SqlConnection conn = new SqlConnection(connString))
                        {
                            conn.Open();
                            string query1 = "Insert into dbo.Admins (dbo.Admins.Fname,dbo.Admins.Lname,dbo.Admins.UserId) Values (@fname,@lname,@userid)";
                            SqlCommand sqlcmd = new SqlCommand(query1, conn);
                            sqlcmd.Parameters.AddWithValue("@fname", collection["firstName"].ToString());
                            sqlcmd.Parameters.AddWithValue("@lname", collection["lastName"].ToString());
                            sqlcmd.Parameters.AddWithValue("@userid", collection["userid"].ToString());
                            sqlcmd.ExecuteNonQuery();
                            sqlcmd.CommandText = "Select @@Identity"; //get the id on new inserted row
                            int newadminid = Convert.ToInt32(sqlcmd.ExecuteScalar());


                            ////update aspnetusers table also with new admin id
                            //string query2 = "Update dbo.AspNetUsers SET dbo.AspNetUsers.AdminID = @adminid where dbo.AspNetUsers.id=@userid ";
                            //SqlCommand sqlcmd2 = new SqlCommand(query2, conn);
                            //sqlcmd2.Parameters.AddWithValue("@adminid", newadminid);
                            //sqlcmd2.Parameters.AddWithValue("@userid", collection["userid"].ToString());
                            //sqlcmd2.ExecuteNonQuery();
                            conn.Close();
                        }
                    }
                }
                else
                {
                    //if yes -> check if current status is approved or not approved
                    var status = collection["status"];
                    if (status == "Not Approved")
                    {
                        //update adminid to null in aspnetusers table and delete the id from Admins table
                        using (SqlConnection conn = new SqlConnection(connString))
                        {
                            conn.Open();
                            string query1 = "Update dbo.AspNetUsers SET dbo.AspNetUsers.AdminID = @adminid where dbo.AspNetUsers.id=@userid ";
                            SqlCommand sqlcmd = new SqlCommand(query1, conn);
                            sqlcmd.Parameters.AddWithValue("@adminid", DBNull.Value);
                            sqlcmd.Parameters.AddWithValue("@userid", collection["userid"].ToString());
                            sqlcmd.ExecuteNonQuery();


                            //string query2 = "Delete from dbo.Admins where dbo.Admins.UserId=@userid ";
                            //SqlCommand sqlcmd2 = new SqlCommand(query2, conn);
                            //sqlcmd2.Parameters.AddWithValue("@userid", collection["userid"].ToString());
                            //sqlcmd2.ExecuteNonQuery();
                            conn.Close();
                        }
                    }
                    else
                    {
                        //get the admin id from admins table and update aspnetusers table

                        using (SqlConnection conn = new SqlConnection(connString))
                        {
                            conn.Open();
                            string query1 = "Select AdminId from dbo.Admins where dbo.Admins.UserId=@userid ";
                            SqlCommand sqlcmd = new SqlCommand(query1, conn);
                            sqlcmd.Parameters.AddWithValue("@userid", collection["userid"].ToString());
                            int adminid = Convert.ToInt32(sqlcmd.ExecuteScalar());


                            string query2 = "Update dbo.AspNetUsers SET dbo.AspNetUsers.AdminID = @adminid where dbo.AspNetUsers.id=@userid ";
                            SqlCommand sqlcmd2 = new SqlCommand(query2, conn);
                            sqlcmd2.Parameters.AddWithValue("@adminid", adminid);
                            sqlcmd2.Parameters.AddWithValue("@userid", collection["userid"].ToString());
                            sqlcmd2.ExecuteNonQuery();
                            conn.Close();
                        }
                    }
                    
                }
                sqlcon.Close();
            }
            return RedirectToAction("SuperAdminIndex");
        }

        //Super admin login 
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> SuperAdminLogin(string returnUrl = null)
        {
            // Clear the existing external cookie to ensure a clean login process
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SuperAdminLogin(SuperAdminLoginViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    _logger.LogInformation("User logged in.");
                    //return RedirectToLocal(returnUrl);
                    return RedirectToAction("SuperAdminIndex");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                    return View(model);
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

       
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}