﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using RiversideHealth.Models;
using RiversideHealth.Models.ViewModels;
using RiversideHealth.Data;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;

namespace RiversideHealth.Controllers
{
    public class TransactionController : Controller
    {
        private readonly RiversideHealthDbContext context;
        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public TransactionController(RiversideHealthDbContext ctx, UserManager<ApplicationUser> userManager)
        {
            context = ctx;
            _userManager = userManager;
        }

        public async Task<ActionResult> Index(int page)
        {
            var admin = await GetCurrentUserAsync();
            if (admin != null)
            {
                if (admin.AdminId == null)
                {
                    ViewData["UserIsAdmin"] = "False";
                    // redirect if user isnt logged in
                   // return Redirect("/Account/Login");
                }
                else
                {
                    ViewData["UserIsAdmin"] = admin.AdminId.ToString();
                }
            }
            else
            {
                ViewData["UserIsAdmin"] = "False";
                // redirect if user isnt logged in
               // return Redirect("/Account/Login");
            }

            /*Pagination Algorithm*/
            // I based this on christine's code
            var myCategories = await context.Transactions.ToListAsync();
            int pagecount = myCategories.Count();
            int pageItems = 8;
            int limit = (int)Math.Ceiling((decimal)pagecount / pageItems) - 1;

            limit = limit < 0 ? 0 : limit;
            page = page < 0 ? 0 : page;
            page = page > limit ? limit : page;

            int initial = pageItems * page;

            ViewData["page"] = (int)page;
            ViewData["PageSummary"] = "";

            if (limit > 0)
            {
                ViewData["PageSummary"] =
                    (page + 1).ToString() + " of " +
                    (limit + 1).ToString();
            }

            List<Transaction> transactionList = await context.Transactions.Skip(initial).Take(pageItems).ToListAsync();
            return View(transactionList);
        }

        public ActionResult Details(int id)
        {
            return View(context.Transactions.Find(id));
        }
    }
}