﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RiversideHealth.Models;

namespace RiversideHealth.Controllers
{
    public class SubscribeController : Controller
    {
        string connString = Global.connString; // @"Data Source = (localdb)\mssqllocaldb; Initial Catalog = RiversideHealth; Integrated Security=True";
        // GET: Subscribe
        public ActionResult Index()
        {
            return View();
        }

        // GET: Subscribe/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Subscribe/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Subscribe/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SubscribeModel subscribeModel)
        {
            using (SqlConnection sqlcon = new SqlConnection(connString))
            {
                sqlcon.Open();
                string quary = "INSERT INTO EmailUpdateForm VALUES(@fName,@lName,@Email,@Date)";
                SqlCommand sqlcmd = new SqlCommand(quary, sqlcon);
                sqlcmd.Parameters.AddWithValue("@fName", subscribeModel.Fname);
                sqlcmd.Parameters.AddWithValue("@lName", subscribeModel.Lname);
                sqlcmd.Parameters.AddWithValue("@Email", subscribeModel.Email);
                DateTime dateTime = DateTime.Now;
                sqlcmd.Parameters.AddWithValue("@Date", dateTime);
                sqlcmd.ExecuteNonQuery();
            }

            return RedirectToAction("Create");
        }

        // GET: Subscribe/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Subscribe/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Subscribe/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Subscribe/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}