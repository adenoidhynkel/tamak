﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using RiversideHealth.Models;
using RiversideHealth.Models.ViewModels;
using RiversideHealth.Data;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;

namespace RiversideHealth.Controllers
{
    public class HealthTipsController : Controller
    {
        private readonly RiversideHealthDbContext context;
        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public HealthTipsController(RiversideHealthDbContext ctx, UserManager<ApplicationUser> userManager)
        {
            context = ctx;
            _userManager = userManager;
        }

        public async Task<ActionResult> Index(int page)
        {
            var admin = await GetCurrentUserAsync();
            if (admin != null)
            {
                if (admin.AdminId == null)
                {
                    ViewData["UserIsAdmin"] = "False";
                }
                else
                {
                    ViewData["UserIsAdmin"] = admin.AdminId.ToString();
                }
            }
            else
            {
                ViewData["UserIsAdmin"] = "False";
            }

            var myCategories = await context.HealthTips.ToListAsync();
            int pagecount = myCategories.Count();
            int pageItems = 8;
            int limit = (int)Math.Ceiling((decimal)pagecount / pageItems) - 1;

            limit = limit < 0 ? 0 : limit;
            page = page < 0 ? 0 : page;
            page = page > limit ? limit : page;

            int initial = pageItems * page;

            ViewData["page"] = (int)page;
            ViewData["PageSummary"] = "";

            if (limit > 0)
            {
                ViewData["PageSummary"] =
                    (page + 1).ToString() + " of " +
                    (limit + 1).ToString();
            }

            List<HealthTips> ht = await context.HealthTips.Skip(initial).Take(pageItems).ToListAsync();

            //return the view
            return View(ht);
        }

        public async Task<ActionResult> Create()
        {
            var admin = await GetCurrentUserAsync();
            if (admin != null)
            {
                if (admin.AdminId == null)
                {
                    ViewData["UserIsAdmin"] = "False";
                    // redirect if user isnt logged in
                    //return Redirect("/Account/Login");
                }
                else
                {
                    ViewData["UserIsAdmin"] = admin.AdminId.ToString();
                }
            }
            else
            {
                ViewData["UserIsAdmin"] = "False";
                // redirect if user isnt logged in
                //return Redirect("/Account/Login");
            }

            HealthtipEdit he = new HealthtipEdit();
            //assign contents
            he.HealthTipsCategory = context.HealthTipsCategories.ToList();

            // pass the view model into the view
            return View(he);
        }

        [HttpPost]
        public ActionResult Create(string tipsTitle, string tipsDescription, int tipsStatus, int tipsCategoryId)
        {
            // create an entity and assign the values
            HealthTips ht = new HealthTips()
            {
                TipsTitle = tipsTitle,
                TipsDescription = tipsDescription,
                TipStatus = tipsStatus,
                TipsCategoryId = tipsCategoryId
            };

            //insert query
            context.HealthTips.Add(ht);

            //save
            context.SaveChanges();
            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Edit(int id)
        {
            var admin = await GetCurrentUserAsync();
            if (admin != null)
            {
                if (admin.AdminId == null)
                {
                    ViewData["UserIsAdmin"] = "False";
                    // redirect if user isnt logged in
                    //return Redirect("/Account/Login");
                }
                else
                {
                    ViewData["UserIsAdmin"] = admin.AdminId.ToString();
                }
            }
            else
            {
                ViewData["UserIsAdmin"] = "False";
                // redirect if user isnt logged in
                //return Redirect("/Account/Login");
            }

            HealthtipEdit he = new HealthtipEdit();
            //assign contents
            he.HealthTips = context.HealthTips.Find(id);
            he.HealthTipsCategory = context.HealthTipsCategories.ToList();

            // pass the view model to the view
            return View(he);
        }

        [HttpPost]
        public ActionResult Edit(int id, string tipsTitle_update, string tipsDescription_update, int tipsStatus_update, int tipsCategoryId_update)
        {
            // create new entity object
            HealthTips ht = new HealthTips()
            {
                TipsId = id
            };

            // attach the obect so we can edit it
            context.HealthTips.Attach(ht);

            // add the updates
            ht.TipsTitle = tipsTitle_update;
            ht.TipsDescription = tipsDescription_update;
            ht.TipStatus = tipsStatus_update;
            ht.TipsCategoryId = tipsCategoryId_update;

            // save to database
            context.SaveChanges();

            return Redirect("/HealthTips/Details/" + id);
        }

        public ActionResult Details(int id)
        {
            HealthtipEdit he = new HealthtipEdit();
            //assign contents
            he.HealthTips = context.HealthTips.Find(id);

            // include only the category of the health tip
            he.HealthTipsCategory = context.HealthTipsCategories.Where(htc => htc.TipsCategoryId == he.HealthTips.TipsCategoryId).ToList();

            // pass the view model
            return View(he);
        }

        public ActionResult Delete(int id)
        {
            // create new entity object
            // reference https://davidsekar.com/c-sharp/modify-or-delete-entity-object-without-select

            HealthTips ht = new HealthTips() { TipsId = id };

            // attach to entity
            context.HealthTips.Attach(ht);

            // remove the record
            context.HealthTips.Remove(ht);

            // save the changes
            context.SaveChanges();

            // redirect to index
            return RedirectToAction("Index");
        }
    }
}