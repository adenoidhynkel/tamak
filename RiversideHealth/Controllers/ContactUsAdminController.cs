﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Data.SqlClient;
using RiversideHealth.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using RiversideHealth.Models.ViewModels;
using RiversideHealth.Data;

namespace RiversideHealth.Controllers
{
    public class ContactUsAdminController : Controller
    {
        private readonly RiversideHealthDbContext db;
        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public ContactUsAdminController(RiversideHealthDbContext context, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _userManager = usermanager;
        }

        string connString = Global.dbConnString; // @"Data Source = (localdb)\mssqllocaldb; Initial Catalog = RiversideHealth; Integrated Security=True";
        [HttpGet]
        public async Task<ActionResult> AdminIndex(int pagenum)
        {
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                if (user.AdminId == null) { ViewData["UserIsAdmin"] = "false"; }
                else { ViewData["UserIsAdmin"] = user.AdminId.ToString(); }
            }
            else
            {
                ViewData["UserIsAdmin"] = "false";
            }

            var _contactUsModels = await db.contactUsAdminModels.ToListAsync();
            
            int pagecount = _contactUsModels.Count();
            int perpage = 3;
            int maxpage = (int)Math.Ceiling((decimal)pagecount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }
            List<ContactUsAdminModel> conusAdminModel = await db.contactUsAdminModels.Skip(start).Take(perpage).ToListAsync();
            return View(conusAdminModel);
            
        }

       
        // GET: ContactUsAdmin/Edit/5
        public ActionResult AdminEdit(int id)
        {
            return View(db.contactUsAdminModels.SingleOrDefault(p => p.Id == id));
        }

        // POST: ContactUsAdmin/AdminEdit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AdminEdit(ContactUsAdminModel contactUsAdminModel)
        {
            using (SqlConnection sqlcon = new SqlConnection(connString))
            {
                sqlcon.Open();
                string quary = "UPDATE Contactus SET Name = @Name,Email = @Email,Question = @Questions,Date=@Date,Status = @Status where id=@id";
                SqlCommand sqlcmd = new SqlCommand(quary, sqlcon);
                sqlcmd.Parameters.AddWithValue("@Name", contactUsAdminModel.Name);
                sqlcmd.Parameters.AddWithValue("@Email", contactUsAdminModel.Email);
                sqlcmd.Parameters.AddWithValue("@Questions", contactUsAdminModel.Question);
                DateTime dateTime = DateTime.Now;
                string datestr = dateTime.ToString("MM-dd-yyyy");
                //sqlcmd.Parameters.AddWithValue("@Date", datestr);
                sqlcmd.Parameters.AddWithValue("@Date", contactUsAdminModel.Date.ToString("MM-dd-yyyy"));
                sqlcmd.Parameters.AddWithValue("@Status", contactUsAdminModel.Status);
                sqlcmd.Parameters.AddWithValue("@id", contactUsAdminModel.Id);
                sqlcmd.ExecuteNonQuery();
            }
            return RedirectToAction("AdminIndex");
        }

        [HttpGet]
        public ActionResult UserCreate()
        {
            return View();
        }

        // POST: ContactUs/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UserCreate(ContactUsAdminModel contactUsAdminModel)
        {
            using (SqlConnection sqlcon = new SqlConnection(connString))
            {
                sqlcon.Open();
                string quary = "INSERT INTO Contactus(Name,Email,Question,Date,Status) VALUES(@Name,@Email,@Questions,@Date,@Status)";
                SqlCommand sqlcmd = new SqlCommand(quary, sqlcon);
                sqlcmd.Parameters.AddWithValue("@Name", contactUsAdminModel.Name);
                sqlcmd.Parameters.AddWithValue("@Email", contactUsAdminModel.Email);
                sqlcmd.Parameters.AddWithValue("@Questions", contactUsAdminModel.Question);
                //DateTime dateTime = DateTime.Now;
                //string datestr = dateTime.ToString("MM-dd-yyyy");
                sqlcmd.Parameters.AddWithValue("@Date", DateTime.UtcNow.ToShortDateString());
                sqlcmd.Parameters.AddWithValue("@Status", false);
                sqlcmd.ExecuteNonQuery();
            }
            return RedirectToAction("Index", "Home");
        }
    }
}