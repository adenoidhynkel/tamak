﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using RiversideHealth.Models;
using RiversideHealth.Models.ViewModels;
using RiversideHealth.Data;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;

namespace RiversideHealth.Controllers
{

    namespace RiversideHealth.Controllers
    {
        public class NewsController : Controller
        {
            private readonly RiversideHealthDbContext db;
            //private readonly ActionResult _env;
            private readonly UserManager<ApplicationUser> _userManager;
            private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

            public NewsController(RiversideHealthDbContext context, UserManager<ApplicationUser> usermanager)
            {
                db = context;
                //_env = env;
                _userManager = usermanager;
            }

            //can we get to Views/News/Index.cshtml

            //can we see the actual database records of news?
            public ActionResult Index()
            {
                List<News> news = db.News.ToList();

                return View(news);

            }

            //can I write a controller method when the user inputs
            //localhost:436:/News/new
            //it then shows the view of Views/News/New.cshtml

            // public ActionResult Edit(int id)
            // {
            //    News news = db.News.Find(id);
            //   if (news == null)
            //  {
            //      Debug.WriteLine("Not found!");
            //      return NotFound();
            //  }

            // return View(news);

            public async Task<ActionResult> Edit(int? id)
            {
                var user = await GetCurrentUserAsync();
                if (user != null)
                {
                    if (user.AdminId == null) { ViewData["UserHasAdmin"] = "False"; }
                    else { ViewData["UserHasAdmin"] = user.AdminId.ToString(); }
                }
                else
                {
                    ViewData["UserHasAdmin"] = "None";
                }
                PageEdit pageEditView = new PageEdit();

                pageEditView.Page = db.Pages
                    .Include(p => p.Admin)
                    .SingleOrDefault(p => p.PageId == id);

                pageEditView.Admins = db.Admins.ToList();
                return View(pageEditView);
            }

            [HttpPost]
            public async Task<ActionResult> Edit(string newsid, string AdminId, int newsDate, int newsMessage)
            {
                var user = await GetCurrentUserAsync();
                if (user != null)
                {
                    if (user.AdminId == null) { ViewData["UserHasAdmin"] = "False"; }
                    else { ViewData["UserHasAdmin"] = user.AdminId.ToString(); }
                }
                else
                {
                    ViewData["UserHasAdmin"] = "None";
                }

                if (db.Pages.Find(newsid) == null)
                {
                    //Show error message
                    return NotFound();

                }
                string query = "update pages set newsid = @newsid, AdminId = @AdminId, newsDate = @newsDate, newsMessage = @newsMessage";

                SqlParameter[] sqlParameters = new SqlParameter[4];
                sqlParameters[0] = new SqlParameter("@newsid", newsid);
                sqlParameters[1] = new SqlParameter("@AdminId", AdminId);
                sqlParameters[2] = new SqlParameter("@newsDate", newsDate);
                sqlParameters[3] = new SqlParameter("@newsMessage", newsMessage);


                db.Database.ExecuteSqlCommand(query, sqlParameters);


                return RedirectToAction("News");
            }

            public ActionResult Delete(int id)
            {
                string query = "delete from pages where pageid = @id";
                db.Database.ExecuteSqlCommand(query, new SqlParameter("@id", id));

                return RedirectToAction("Index");
            }

                   

              
            }
        }

    }
/*public async Task <ActionResult> Create()
            {
                var user = await GetCurrentUserAsync();
                if (user !=null)
                {
                    if (user.AdminId == null) { ViewData["UserHasAdmin"] = "False";
                    else { ViewData["UserhasAdmion"] = user.AdminId.ToString(); }
                }
                else
                {
                    ViewData["UserHasAdmin"] = "None";
                }
                */