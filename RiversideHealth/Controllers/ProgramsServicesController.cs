﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using RiversideHealth.Models;
using RiversideHealth.Models.ViewModels;
using RiversideHealth.Data;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;

//References Christine's blog controller.
namespace RiversideHealth.Controllers
{
    public class ProgramsServicesController : Controller
    {
        private readonly RiversideHealthDbContext db;
        private readonly UserManager<ApplicationUser> _userManager;
        //private int start;
        //private int perpage;

        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public ProgramsServicesController(RiversideHealthDbContext context, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _userManager = usermanager;
        }
        public async Task <ActionResult> Index(int pagenum)
        {
            var ProgramServices = await db.ProgramServices.Include(a => a.Admin).ToListAsync();
            int programservicescount = ProgramServices.Count();
            int perpage = 4;
            int maxpage = (int)Math.Ceiling((decimal)programservicescount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }
           // List<Blog> blogs = await db.Blogs.Include(b => b.Author).Skip(start).Take(perpage).ToListAsync();
            List<ProgramServices>programServices = await db.ProgramServices.Include(a => a.Admin).Skip(start).Take(perpage).ToListAsync();
            
            return View(ProgramServices);
        }
            public async Task<ActionResult> Edit(int? id)
        {
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                if (user.AdminId == null) { ViewData["UserHasAdmin"] = "False"; }
                else { ViewData["userHasAdmin"] = user.AdminId.ToString(); }
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
            }
            PageEdit pageEditView = new PageEdit();{

            pageEditView.Page = db.Pages
                .Include(p => p.Admin)
                .SingleOrDefault(p => p.PageId == id);

            pageEditView.Admins = db.Admins.ToList();
            return View(pageEditView);
            } 

        }

        [HttpPost]
        public async Task<ActionResult> Edit(string ProgramServicesId, string AdminId, string ProgramServicesHours, string ProgramServicesName)
        {
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                if (user.Admin == null) { ViewData["userHasAdmin"] = "False"; }
                else { ViewData["UserHasAdmin"] = user.AdminId.ToString(); }
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
            }

            if (db.Pages.Find(ProgramServicesId) == null)
            {
                return NotFound();
            }
            string query = "update pages set ProgramServicesId @ProgramServicesId, AdminId = @AdminId, ProgramServicesHours = @ProgramServicesHours,  ProgramServicesName = ProgramServicesName";

            SqlParameter[] sqlParameters = new SqlParameter[4];
            sqlParameters[0] = new SqlParameter("@ProgramServicesId", ProgramServicesId);
            sqlParameters[0] = new SqlParameter("@AdminId", AdminId);
            sqlParameters[0] = new SqlParameter("@ProgramServicesHours", ProgramServicesHours);
            sqlParameters[0] = new SqlParameter("@ProgramServicesName", ProgramServicesName);

            db.Database.ExecuteSqlCommand(query, sqlParameters);

            return RedirectToAction("ProgramServices");

        }

        public ActionResult Delete(int id)
        {
            string query = "delete from pages where pageid = @id";
            db.Database.ExecuteSqlCommand(query, new SqlParameter("@id", id));

            return RedirectToAction("Index");
        }

            public ActionResult PublicIndex()
        {
            return View(db.ProgramServices);
        }

        public async Task<ActionResult> Create()
        {
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                if (user.AdminId == null) { ViewData["UserHasAdmin"] = "False"; }
                else { ViewData["UserHasAdmin"] = user.AdminId.ToString(); }
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
            }

            ProgramServicesEdit programServicesEdit = new ProgramServicesEdit();
            programServicesEdit.Admins = db.Admins.ToList();
            return View(programServicesEdit);
        }


        [HttpPost]
        public ActionResult Create(string ServiceName, string ServiceHours)
        {
            string query = "insert into Careers (serviceName, serviceHours) values (@serviceName, @serviceHours)";
            SqlParameter[] myparams = new SqlParameter[2];
            myparams[0] = new SqlParameter("@servicesName", ServiceName);
            myparams[1] = new SqlParameter("@serviceHours", ServiceHours);

            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("ProgramsServices");
        }

  
    }
}
    
    