﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RiversideHealth.Models;
using System.Diagnostics;

namespace RiversideHealth.Controllers
{
    public class PatientsAndVisitorsAdminController : Controller
    {
        string connString = Global.connString; //@"Data Source = (localdb)\mssqllocaldb; Initial Catalog = RiversideHealth; Integrated Security=True";
        // GET: PatientsAndVisitorsAdmin
        public ActionResult VisitorIndex()
        {
            Debug.WriteLine("where is my visitor page");
            DataTable dbblContact = new DataTable();
            using (SqlConnection sqlcon = new SqlConnection(connString))
            {
                sqlcon.Open();
                SqlDataAdapter sqlda = new SqlDataAdapter("SELECT * FROM VisitorInfo", sqlcon);
                sqlda.Fill(dbblContact);
            }
            return View(dbblContact);
        }

        // GET: PatientsAndVisitorsAdmin/Create
        public ActionResult VisitorCreate()
        {
            return View();
        }

        // POST: PatientsAndVisitorsAdmin/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult VisitorCreate(VisitorInfoModel visitorInfoModel)
        {
            using (SqlConnection sqlcon = new SqlConnection(connString))
            {
                sqlcon.Open();
                string query = "INSERT INTO VisitorInfo VALUES(@Title,@Description,@Date)";
                SqlCommand sqlcmd = new SqlCommand(query, sqlcon);
                sqlcmd.Parameters.AddWithValue("@Title", visitorInfoModel.Title);
                sqlcmd.Parameters.AddWithValue("@Description", visitorInfoModel.Description );
                DateTime dateTime = DateTime.Now;
                string dateonly = dateTime.ToString("MM-dd-yyyy");
                sqlcmd.Parameters.AddWithValue("@Date", dateonly);
                sqlcmd.ExecuteNonQuery();
            }
            return RedirectToAction("VisitorIndex");
        }

        // GET: PatientsAndVisitorsAdmin/Edit/5
        public ActionResult VisitorEdit(int id)
        {
            VisitorInfoModel vim = new VisitorInfoModel();
            DataTable datatable = new DataTable();
            using (SqlConnection sqlcon = new SqlConnection(connString))
            {
                sqlcon.Open();
                SqlDataAdapter sqlda = new SqlDataAdapter("SELECT * FROM VisitorInfo where VisitorInfoId = @Id", sqlcon);
                sqlda.SelectCommand.Parameters.AddWithValue("@Id", id);
                sqlda.Fill(datatable);
            }
            if (datatable.Rows.Count == 1)
            {
                vim.Id = Convert.ToInt32(datatable.Rows[0][0].ToString());
                vim.Title = datatable.Rows[0][1].ToString();
                vim.Description = datatable.Rows[0][2].ToString();
                vim.Date = datatable.Rows[0][3].ToString();
                return View(vim);
            }
            else
                return RedirectToAction("VisitorIndex");
        }

        // POST: PatientsAndVisitorsAdmin/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult VisitorEdit(VisitorInfoModel vim)
        {
            using (SqlConnection sqlcon = new SqlConnection(connString))
            {
                sqlcon.Open();
                string quary = "Update VisitorInfo SET VInfoTitle=@Title , VDescription=@Description ,  Date=@Date WHERE VisitorInfoId=@Id";
                SqlCommand sqlcmd = new SqlCommand(quary, sqlcon);
                sqlcmd.Parameters.AddWithValue("@Id", vim.Id);
                sqlcmd.Parameters.AddWithValue("@Title", vim.Title);
                sqlcmd.Parameters.AddWithValue("@Description", vim.Description);
                DateTime dateTime = Convert.ToDateTime(vim.Date).Date;
                string onlydate = dateTime.ToString("MM-dd-yyyy");
                sqlcmd.Parameters.AddWithValue("@Date", onlydate);
                sqlcmd.ExecuteNonQuery();
            }
            return RedirectToAction("VisitorIndex");
        }

        // GET: PatientsAndVisitorsAdmin/Delete/5
        public ActionResult VisitorDelete(int id)
        {
            using (SqlConnection sqlcon = new SqlConnection(connString))
            {
                sqlcon.Open();
                string quary = "DELETE FROM VisitorInfo WHERE VisitorInfoId=@Id ";
                SqlCommand sqlcmd = new SqlCommand(quary, sqlcon);
                sqlcmd.Parameters.AddWithValue("@Id", id);
                sqlcmd.ExecuteNonQuery();
            }
            return RedirectToAction("VisitorIndex");
        }

        //Patient 
        public ActionResult PatientIndex()
        {
            DataTable dbblContact = new DataTable();
            using (SqlConnection sqlcon = new SqlConnection(connString))
            {
                sqlcon.Open();
                SqlDataAdapter sqlda = new SqlDataAdapter("SELECT * FROM PatientInfo", sqlcon);
                sqlda.Fill(dbblContact);
            }
            return View(dbblContact);
        }

        // GET: PatientsAndVisitorsAdmin/Create
        public ActionResult PatientCreate()
        {
            return View();
        }

        // POST: PatientsAndVisitorsAdmin/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PatientCreate(PatientInfoModel patientInfoModel)
        {
            using (SqlConnection sqlcon = new SqlConnection(connString))
            {
                sqlcon.Open();
                string quary = "INSERT INTO PatientInfo VALUES(@Title,@Description,@Date)";
                SqlCommand sqlcmd = new SqlCommand(quary, sqlcon);
                sqlcmd.Parameters.AddWithValue("@Title", patientInfoModel.Title);
                sqlcmd.Parameters.AddWithValue("@Description", patientInfoModel.Description);
                DateTime dateTime = DateTime.Now;
                string dateonly = dateTime.ToString("MM-dd-yyyy");
                sqlcmd.Parameters.AddWithValue("@Date", dateonly);
                sqlcmd.ExecuteNonQuery();
            }
            return RedirectToAction("PatientIndex");
        }

        // GET: PatientsAndVisitorsAdmin/Edit/5
        public ActionResult PatientEdit(int id)
        {
            PatientInfoModel pim = new PatientInfoModel();
            DataTable datatable = new DataTable();
            using (SqlConnection sqlcon = new SqlConnection(connString))
            {
                sqlcon.Open();
                SqlDataAdapter sqlda = new SqlDataAdapter("SELECT * FROM PatientInfo where PatientInfoId = @Id", sqlcon);
                sqlda.SelectCommand.Parameters.AddWithValue("@Id", id);
                sqlda.Fill(datatable);
            }
            if (datatable.Rows.Count == 1)
            {
                pim.Id = Convert.ToInt32(datatable.Rows[0][0].ToString());
                pim.Title = datatable.Rows[0][1].ToString();
                pim.Description = datatable.Rows[0][2].ToString();
                pim.Date = datatable.Rows[0][3].ToString();
                return View(pim);
            }
            else
                return RedirectToAction("PatientIndex");
        }

        // POST: PatientsAndVisitorsAdmin/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PatientEdit(PatientInfoModel pim)
        {
            using (SqlConnection sqlcon = new SqlConnection(connString))
            {
                //i suspect that the problem is in the function
                Debug.WriteLine("I know for sure that I am in the patientedit function");
                sqlcon.Open();
                string query = "Update PatientInfo SET PInfoTitle=@Title , PDescription=@Description ,  Date=@Date WHERE PatientInfoId=@Id";
                SqlCommand sqlcmd = new SqlCommand(query, sqlcon);
                sqlcmd.Parameters.AddWithValue("@Id", pim.Id);
                sqlcmd.Parameters.AddWithValue("@Title", pim.Title);
                sqlcmd.Parameters.AddWithValue("@Description", pim.Description);
                DateTime dateTime = Convert.ToDateTime(pim.Date).Date;
                string onlydate = dateTime.ToString("MM-dd-yyyy");
                sqlcmd.Parameters.AddWithValue("@Date", onlydate);
                sqlcmd.ExecuteNonQuery();
            }
            return RedirectToAction("PatientIndex");
        }

        // GET: PatientsAndVisitorsAdmin/Delete/5
        public ActionResult PatientDelete(int id)
        {
            using (SqlConnection sqlcon = new SqlConnection(connString))
            {
                sqlcon.Open();
                string query = "DELETE FROM PatientInfo WHERE PatientInfoId=@Id ";
                SqlCommand sqlcmd = new SqlCommand(query, sqlcon);
                sqlcmd.Parameters.AddWithValue("@Id", id);
                sqlcmd.ExecuteNonQuery();
            }
            return RedirectToAction("PatientIndex");
        }


    }
}