﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace RiversideHealth.Controllers
{
    public class PatientsAndVisitorsController : Controller
    {
        string connString = Global.connString; //@"Data Source = (localdb)\mssqllocaldb; Initial Catalog = RiversideHealth; Integrated Security=True";
        public ActionResult Start()
        {
            return View();
        }

        public ActionResult VisitorInfo()
        {
            DataTable dbblContact = new DataTable();
            using (SqlConnection sqlcon = new SqlConnection(connString))
            {
                sqlcon.Open();
                SqlDataAdapter sqlda = new SqlDataAdapter("SELECT * FROM VisitorInfo", sqlcon);
                sqlda.Fill(dbblContact);
            }
            return View(dbblContact);
        }

        public ActionResult PatientInfo()
        {
            DataTable dbblContact = new DataTable();
            using (SqlConnection sqlcon = new SqlConnection(connString))
            {
                sqlcon.Open();
                SqlDataAdapter sqlda = new SqlDataAdapter("SELECT * FROM PatientInfo", sqlcon);
                sqlda.Fill(dbblContact);
            }
            return View(dbblContact);
        }

        // GET: PatientsAndVisitors
        public ActionResult Index()
        {
            return View();
        }

        // GET: PatientsAndVisitors/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: PatientsAndVisitors/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PatientsAndVisitors/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: PatientsAndVisitors/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: PatientsAndVisitors/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: PatientsAndVisitors/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: PatientsAndVisitors/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}