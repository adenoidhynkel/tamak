﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RiversideHealth.Models
{
    public class ContactUsAdminModel
    {
        [Key]
        public int Id { get; set; }        

        [Required, StringLength(100), Display(Name = "Name")]
        public string Name { get; set; }

        [Required, StringLength(100), Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required, StringLength(1000), Display(Name = "Question")]
        public string Question { get; set; }

        
        [DataType(DataType.DateTime), Display(Name = "Date")]
        public DateTime Date { get; set; }

        //0 for inactive, 1 for active. If position is filled, don't display on careers page.
        [Display(Name = "Status")]
        public bool Status { get; set; }

    }


}
