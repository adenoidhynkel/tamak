﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RiversideHealth.Models
{
    public class HealthTipsCategory
    {
        [Key]
        public int TipsCategoryId { get; set; }

        [Required, StringLength(255), Display(Name = "CategoryTitle")]
        public string CategoryTitle { get; set; }

        [InverseProperty("HealthTipsCategory")]
        public virtual List<HealthTips> HealthTips { get; set; }

    }
}
