﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RiversideHealth.Models
{
    public class Slider
    {
        [Key]
        public int SliderId { get; set; }

        [Required, StringLength(50), Display(Name = "Slider Title")]
        public string SliderTitle { get; set; }

        [Required, StringLength(150), Display(Name = "Slider Description")]
        public string SliderDescription { get; set; }

        // set default values
        // reference: https://stackoverflow.com/questions/23823103/default-value-in-mvc-model-using-data-annotation
        // author: Nilesh, Jeppe Stig Nelsen

        [Display(Name = "Add Button")]
        public int hasButton { get; set; } = 0;

        [Required, Display(Name = "Slide Image")]
        public string SliderImage { get; set; } = "Default.jpg";
    
    }
}
