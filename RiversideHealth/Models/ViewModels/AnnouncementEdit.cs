﻿//References Christine's code.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RiversideHealth.Models.ViewModels
{
    public class AnnouncementEdit
    {
        public AnnouncementEdit()
        {

        }

        public virtual Announcement Announcement { get; set; }

        public IEnumerable<Admin> Admins { get; set; }
    }
}