﻿//References Christine's code.
using System.Collections.Generic;


namespace RiversideHealth.Models.ViewModels
{
    public class PageEdit
    {
        public PageEdit()
        {

        }
        public virtual Page Page { get; set; }

        public IEnumerable<Admin> Admins { get; set; }
    }
}