﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RiversideHealth.Models.ViewModels
{
    public class HealthtipEdit
    {
        public HealthtipEdit() { }

        public virtual HealthTips HealthTips { get; set; }
        public IEnumerable<HealthTipsCategory> HealthTipsCategory { get; set; }
    }
}
