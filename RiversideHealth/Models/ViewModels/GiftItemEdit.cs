﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// referenced from christine's code

namespace RiversideHealth.Models.ViewModels
{
    public class GiftItemEdit
    {
        public GiftItemEdit() { }

        public virtual GiftItems GiftItems { get; set; }

        public IEnumerable<GiftCategory> GiftCategories { get; set; }

        public IEnumerable<Locations> Locations { get; set; }
    }
}
