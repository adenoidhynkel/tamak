﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RiversideHealth.Models
{
    public class HealthTips
    {
        [Key]
        public int TipsId { get; set; }

        [Required, StringLength(50), Display(Name = "Tips Title")]
        public string TipsTitle { get; set; }

        [Required, StringLength(255), Display(Name = "Tips Description")]
        public string TipsDescription { get; set; }

        public int TipStatus { get; set; }

        [ForeignKey("TipsCategoryId")]
        public int TipsCategoryId { get; set; }

        public virtual HealthTipsCategory HealthTipsCategory { get; set; }

    }
}
