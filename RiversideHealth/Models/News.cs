﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace RiversideHealth.Models
{
    public class News
    {
        [Key]
        public int newsid { get; set; }

        [Required, StringLength(200), Display(Name = "News & Updates")]
        public string NewsMessage { get; set; }

        [Required, DataType(DataType.DateTime), Display(Name = "Date")]
        public DateTime NewsDate { get; set; }

        [ForeignKey("AdminId")]
        public int AdminId { get; set; }

        public virtual Admin Admin { get; set; }
    }
}
