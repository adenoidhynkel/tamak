﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RiversideHealth.Models
{
    public class SubscribeModel
    {
        public int Id { get; set; }
        [Required, Display(Name = "First Name")]
        public string Fname { get; set; }
        [Required, Display(Name = "Last Name")]
        public string Lname { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        public string Date { get; set; }
    }
}
