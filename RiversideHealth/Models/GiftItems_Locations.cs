﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RiversideHealth.Models
{
    public class GiftItems_Locations
    {
   
        public int GiftItemsId { get; set; }
        public GiftItems GiftItems { get; set; }

        public int LocationsId { get; set; }
        public Locations Locations { get; set; }

    }
}
