﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RiversideHealth.Models;

namespace RiversideHealth.Data
{
    public static class DbInitializer
    {

        public static void Initialize(RiversideHealthDbContext context)
        {
            //Seed data when DB is created.
            //Referenced Christine's code
            context.Database.EnsureCreated();
            return;
        }
    }
}
